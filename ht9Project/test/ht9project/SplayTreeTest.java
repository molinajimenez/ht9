/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht9project;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author USER
 */
public class SplayTreeTest {
    
    public SplayTreeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }



    /**
     * Test of get method, of class SplayTree.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        
        String key = "hola";
        String value = "adios";
        
        SplayTree instance = new SplayTree();
        instance.put(key, value);
        
        String expResult = "adios";
        Object result = instance.get(key);
        
        assertEquals(expResult, result);

    }

    /**
     * Test of put method, of class SplayTree.
     */
    @Test
    public void testPut() {
        System.out.println("put");
        String key = "hola";
        String value = "adios";
        
        SplayTree instance = new SplayTree();
        instance.put(key, value);
        
        int expResult = 1;
        int result = instance.size();
        
        assertEquals(expResult, result);
        
    }

   
}

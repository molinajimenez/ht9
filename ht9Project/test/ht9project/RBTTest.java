/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht9project;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author USER
 */
public class RBTTest {
    
    public RBTTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }


    /**
     * Test of get method, of class RBT.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        String key = "hola";
        String value = "adios";
        
        RBT instance = new RBT<String, String>();
        instance.put(key, value);
        
        String expResult = "adios";
        Object result = instance.get(key);
        assertEquals(expResult, result);
    }


    /**
     * Test of add method, of class RBT.
     */
    @Test
    public void testPut() {
        System.out.println("put");
        String key = "hola";
        String val = "adios";
        RBT instance = new RBT<String, String>();
        instance.put(key, val);

        int expResult = 1;
        int result = instance.size();
        
        assertEquals(expResult, result);
    }

 
    
}

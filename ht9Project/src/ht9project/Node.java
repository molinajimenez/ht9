
package ht9project;
/**
 * 
 * Clase obtenida y modificada acorde a la implementacion de https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/
 * @author Maria Fernanda Lopez
 * @author Guillermo Sandoval
 * @version 20/04/2018
 */
public class Node<E> {
    private Association<String, String> palabra ;
    private Node<E> padre;
    private Node<E> hijod;
    private Node<E> hijoi;
    
    /**
     * Constructor de la calse Nodo
     * @param ingles palabra en ingles
     * @param espanol  palabra en espanol
     */
    public Node(String ingles, String espanol){
        palabra = new Association<>(ingles, espanol);
        padre = null;
        hijod = null;
        hijoi = null;
        
    
}
    /**
     * constructor vacio para asegurar el funcionamiento de la clase
     */

    public Node() {
        
    }
    
    /**
     * Metodo que devuelve la instacia de association que se vincula con el nodo
     * para la obtención de la palabra en ingles con su debida traduccion
     * @return la palabra en ingles con su traducción en espanol
     */
    public Association<String, String> getVal(){
        return palabra;
    }
    
    /**
     * Método para obtener el Value de la association
     * @return value
     */
    public String getValue(){
        return palabra.theValue;
    }
    
    /**
     * Metodo para obtener la Key de la association
     * @return key
     */
    public String getKey(){
        return palabra.theKey;
              
    }
    
    /**
     * Metodo para establecer el nodo padre del arbol
     * @param padre 
     */
    public void setPadre(Node padre){
        this.padre = padre;
    }
    
    /**
     * metodo para establecer el nodo que se encuentra a la derecha
     * @param left 
     */
    public void setLeft(Node left){
        hijod = left;
        
    }
    
    /**
     * metodo para establecer el nodo que se encuentra a la izquierda
     * @param right 
     */
    public void setRight(Node right){
        hijoi = right;
    }
    
    /**
     * metodo para obtener un hijo de un nodo que se encuentra a la derecha
     * @return 
     */
    public Node<E> getLeft(){
        return hijod;
    }
    
    /**
     * metodo para obtener un hijo de un nodo que se encuentra a la izquierda
     * @return 
     */
    public Node<E> getRight(){
        return hijoi;
    }
    
    /**
     * metodo para obtener el padre de los hijos
     * @return nodo padre
     */
    public Node<E> getPadre(){
        return padre;
    }
    
    /**
     * metodo que realiza la busqueda de la palabra entre los hijos del arbol
     * @param valor palabra a buscar
     * @return palabra que no se encontro en el diccionara
     */
    public String search(String valor){
        if (valor.equals(this.palabra.getKey())){
            
            return this.palabra.theValue;
        }else if (valor.compareTo(this.palabra.getKey()) < 0) {
            
            if (hijod == null){
                return "*" + valor + "*";
            }else{
                return hijod.search(valor);
            }
        }else if(valor.compareTo(this.palabra.getKey()) > 0) {
            
            if (hijoi == null){
                return "*" + valor + "*";
            }else{
                return hijoi.search(valor);

            }
        }
        
        return "*" + valor + "*";
    }
    
    @Override
    public String toString(){
        String oracion = "\t("+ palabra.theKey + ", " + palabra.theValue + ")";
       return oracion;
    }
}
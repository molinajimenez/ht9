/******************************************************************************
 *  Compilation:  javac RedBlackBST.java
 *  Execution:    java RedBlackBST < input.txt
 *  Dependencies: StdIn.java StdOut.java  
 *  Data files:   https://algs4.cs.princeton.edu/33balanced/tinyST.txt  
 *    
 *  A symbol table implemented using a left-leaning red-black BST.
 *  This is the 2-3 version.
 *
 *  Note: commented out assertions because DrJava now enables assertions
 *        by default.
 *
 *  % more tinyST.txt
 *  S E A R C H E X A M P L E
 *  
 *  % java RedBlackBST < tinyST.txt
 *  A 8
 *  C 4
 *  E 12
 *  H 5
 *  L 11
 *  M 9
 *  P 10
 *  R 3
 *  S 0
 *  X 7
 *
 */

package ht9project;
import java.util.NoSuchElementException;

/**
 * INSPIRADO DEL CODIGO DE LOS SIGUIENTES AUTORES:
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */

public class RedBlackTree<E> extends BinarySearchTree<E> {

    private static final boolean RED   = true;
    private static final boolean BLACK = false;
    
    private RedBlackNode root;   // root of the BST


    /**
     * Initializes an empty symbol table.
     */
    public RedBlackTree() {
        root = null;
    }

   /***************************************************************************
    *  Node helper methods.
    ***************************************************************************/
    // is node x red; false if x is null ?
    private boolean isRed(RedBlackNode x) {
        if (x == null) return false;
        return x.getColor() == RED;
    }

    // number of node in subtree rooted at x; 0 if x is null
    private int size(RedBlackNode x) {
        if (x == null) return 0;
        return x.getSize();
    } 


    /**
     * Returns the number of key-value pairs in this symbol table.
     * @return the number of key-value pairs in this symbol table
     */
    public int size() {
        return size(root);
    }

   /**
     * Is this symbol table empty?
     * @return {@code true} if this symbol table is empty and {@code false} otherwise
     */
    public boolean isEmpty() {
        return root == null;
    }


   /***************************************************************************
    *  Standard BST search.
    ***************************************************************************/

    /**
     * Returns the value associated with the given key.
     * @param key the key
     * @return the value associated with the given key if the key is in the symbol table
     *     and {@code null} if the key is not in the symbol table
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public String get(String key) {
        if (key == null) throw new IllegalArgumentException("argument to get() is null");
        return get(root, key);
    }

    // value associated with the given key in subtree rooted at x; null if no such key
    private String get(RedBlackNode x, String key) {
        while (x != null) {
            int cmp = key.compareTo(x.getKey());
            if      (cmp < 0) x = x.getLeft();
            else if (cmp > 0) x = x.getRight();
            else              return x.getValue();
        }
        return null;
    }

    /**
     * Does this symbol table contain the given key?
     * @param key the key
     * @return {@code true} if this symbol table contains {@code key} and
     *     {@code false} otherwise
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public boolean contains(String key) {
        return get(key) != null;
    }

   /***************************************************************************
    *  Red-black tree insertion.
    ***************************************************************************/

    /**
     * Inserts the specified key-value pair into the symbol table, overwriting the old 
     * value with the new value if the symbol table already contains the specified key.
     * Deletes the specified key (and its associated value) from this symbol table
     * if the specified value is {@code null}.
     *
     * @param key the key
     * @param val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(String key, String val) {
        if (key == null) throw new IllegalArgumentException("first argument to put() is null");
        root = put(root, key, val);
        root.setColor(BLACK);
        // assert check();
    }

    // insert the key-value pair in the subtree rooted at h
    private RedBlackNode put(RedBlackNode h, String key, String val) { 
        if (h == null) return new RedBlackNode(key, val, RED, 1);

        int cmp = key.compareTo(h.getKey());
        if      (cmp < 0) h.setLeft(put(h.left,  key, val)); 
        else if (cmp > 0) h.setRight(put(h.right, key, val)); 
        else              h.setValue(val);

        // fix-up any right-leaning links
        if (isRed(h.getRight()) && !isRed(h.getLeft()))      h = rotateLeft(h);
        if (isRed(h.getLeft())  &&  isRed(h.getLeft().getLeft())) h = rotateRight(h);
        if (isRed(h.getLeft())  &&  isRed(h.getRight()))     flipColors(h);
        h.setSize(size(h.left) + size(h.right) + 1);

        return h;
    }
   /***************************************************************************
    *  Red-black tree helper functions.
    ***************************************************************************/

    // make a left-leaning link lean to the right
    private RedBlackNode rotateRight(RedBlackNode h) {
        // assert (h != null) && isRed(h.left);
        RedBlackNode x = h.left;
        h.setLeft(x.right);
        x.setRight(h);
        x.setColor(x.right.getColor());
        x.right.setColor(RED);
        x.setSize(h.getSize());
        h.setSize(size(h.left) + size(h.right) + 1);
        return x;
    }

    // make a right-leaning link lean to the left
    private RedBlackNode rotateLeft(RedBlackNode h) {
        // assert (h != null) && isRed(h.right);
        RedBlackNode x = h.right;
        h.setRight(x.left);
        x.setLeft(h);
        x.setColor(x.left.getColor());
        x.left.setColor(RED);
        x.setSize(h.getSize());
        h.setSize(size(h.left) + size(h.right) + 1);
        return x;
    }

    // flip the colors of a node and its two children
    private void flipColors(RedBlackNode h) {
        // h must have opposite color of its two children
        // assert (h != null) && (h.left != null) && (h.right != null);
        // assert (!isRed(h) &&  isRed(h.left) &&  isRed(h.right))
        //    || (isRed(h)  && !isRed(h.left) && !isRed(h.right));
        h.setColor(!h.getColor());
        h.left.setColor(!h.left.getColor());
        h.right.setColor(!h.right.getColor());
    }

    // Assuming that h is red and both h.left and h.left.left
    // are black, make h.left or one of its children red.
    private RedBlackNode moveRedLeft(RedBlackNode h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.left) && !isRed(h.left.left);

        flipColors(h);
        if (isRed(h.right.left)) { 
            h.setRight(rotateRight(h.right));
            h = rotateLeft(h);
            flipColors(h);
        }
        return h;
    }

    // Assuming that h is red and both h.right and h.right.left
    // are black, make h.right or one of its children red.
    private RedBlackNode moveRedRight(RedBlackNode h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.right) && !isRed(h.right.left);
        flipColors(h);
        if (isRed(h.left.left)) { 
            h = rotateRight(h);
            flipColors(h);
        }
        return h;
    }
    
}

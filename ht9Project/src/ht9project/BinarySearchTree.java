/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht9project;

/**
 *
 * @author molin
 */
public class BinarySearchTree<E> {
    
    private Node root;
    private int count = 0;
    
    public BinarySearchTree(){
        //Al crear el arbol, la raiz es null igual que no tiene hijos
        root = null;
    }
    
    /**
     * metodo para insertar un nodo en el arbol
     * @param newNode 
     */
    public void insert(Node newNode) 
    {
        //Inserta un nodo, es decir una hoja
        root = insertRec(root, newNode);
    }
    
    /**
     * metodo para insertar recursivamente un nodo en el arbol
     * @param root
     * @param nodo
     * @return nodo
     */
    public Node insertRec(Node root, Node nodo) 
    {
        
        if (root == null) 
        {
            root = nodo;
            return root;
        }
       
        else if ((root.getKey()).compareTo(nodo.getKey()) > 0) 
        {
            root.setLeft(insertRec(root.getLeft(), nodo)); 
            root.getLeft().setPadre(root); 
        }
        else if ((root.getKey().compareTo(nodo.getKey()) < 0))
        {
            root.setRight(insertRec(root.getRight(), nodo));
            root.getRight().setPadre(root); 
        }
 
       
      return root;
    }
    
    /**
     * metodo para buscar la palabra dentro del arbol
     * @param dato
     * @return 
     */
    public String search(String dato){
        //Para buscar en un arbol tiene complejidad de log (n), si no hay raiz, retorna null
        //Ya que el arbol esta vacio.
        if (root == null){
            
            return "*" + dato + "*";
        }
        //De lo contrario regresa elvalor buscado, de acuerdo a un "match" con el dato
        else{
            return root.search(dato);
        }
    }
    
    /**
     * metodo para ordenar el arbol de forma InOrder
     * @param root 
     */
    public void inorderRec(Node root) {
        //Para el recorrido inorder del arbol, lo hace a partir de ir primero
        //Por el hijo izquierdo, luego por el nodo y luego el derecho.
        if (root != null) {
            inorderRec(root.getLeft());
            System.out.println(root.toString());
            inorderRec(root.getRight());
        }
    }
    
    public void put(E key, E value){ 
        
    }
    
    public E get(E val){ 
        return val;
    }
    
     public int size()
    {
        return count;
    }
    
    public void inorder()  {
        //Para obtener el inorder del arbol
       inorderRec(root);
    }
 
}
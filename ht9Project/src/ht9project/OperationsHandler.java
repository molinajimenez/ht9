/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht9project;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JFileChooser;

/**
 * Clase que libera al main... Consiste en leer
 * @author molin y ze
 */
public class OperationsHandler {
    //ArrayList<String> palabras = new ArrayList<String>();
    ArrayList<Node> palabras = new ArrayList<>();
    SplayTree spt = new SplayTree<String, String>();
    RBT rbt = new RBT<String, String>();
    boolean rbtT = false;
    boolean sptT = false;
    
    public BinarySearchTree printMenu(){
        System.out.println("Hola, esta es una nueva iteracion del programa traductor, en este caso podra utilizar 2 tipos de estructuras:");
        System.out.println("1. RedBlackTree.");
        System.out.println("2. SplayTree.");
        System.out.println("3. Salir.");
        Scanner scanner = new Scanner(System.in);
        String x=scanner.nextLine();
        
        if(x.equals("1")){ 
            rbtT = true;
        }else if(x.equals("2")){ 
            sptT = true;
        }else{ 
            System.exit(0);
        }
        
        Factory builder=new Factory();
        BinarySearchTree structure=builder.getStructure(x);
        return structure;
        
    }
    public void choosenFileDic(BinarySearchTree x) throws FileNotFoundException{
        JFileChooser menu=new JFileChooser();
        int returnV = menu.showOpenDialog(null);
        BufferedReader reader;
        
        if (returnV == JFileChooser.APPROVE_OPTION){ 
        //---Leer archivo--//        
            try {
                File target=menu.getSelectedFile().getAbsoluteFile();
                reader = new BufferedReader(new FileReader(target));  
            
                String leer = "";
            
                while ((leer = reader.readLine()) != null){
                    String[] palabra = leer.split("\\s+");
                    String ingles = palabra[0];
                    String espanol = palabra[1];
                    
                    //Node<Association<String,String>> nodo= new Node(ingles,espanol);
                    if (sptT == true){ 
                        spt.put(ingles, espanol);
                    }else if (rbtT == true){ 
                        rbt.put(ingles, espanol);
                    }

                }
                
                /*for(int i=0; i<palabras.size()-1;i++){
                    //se separan las palabras
                    int lugar=palabras.get(i).indexOf(" ");
                    String ingles=palabras.get(i).substring(1,lugar);
                    String espanol=palabras.get(i).substring(lugar+1,palabras.get(i).length()-1);
                    
                    // add al arbol
                    Node<Association<String,String>> nodo= new Node(ingles,espanol);
                    x.put(nodo);
                }*/
            } catch (IOException ex) {
                System.out.println("ERROR al leer archivo.");
            }     
        } else{ 
            System.out.println("Operacion cancelada.");
            System.exit(0);
        } 
    }
    public void choosenFileToTraduce(BinarySearchTree x){
         JFileChooser menu=new JFileChooser();
        int returnV = menu.showOpenDialog(null);
        BufferedReader readerTraduce;
        if (returnV == JFileChooser.APPROVE_OPTION){ 
        //---Leer archivo--//        
            try {
                File target=menu.getSelectedFile().getAbsoluteFile();
                readerTraduce = new BufferedReader(new FileReader(target));  
                
                 BufferedReader bufferedReader = new BufferedReader(readerTraduce);
                String linea = "";
                Scanner scanner = new Scanner(readerTraduce);
                String palabra = "";
        
                while (scanner.hasNextLine()) {
                    linea += scanner.nextLine();
                }
                        
        String porTraducir[] = linea.split(" ");

        String res="";
        
        String word;
        for(String p: porTraducir){
            word = p.toLowerCase();
            if (sptT == true){ 
                res += spt.get(word) + " ";
            }else if (rbtT == true){ 
                res += rbt.get(word) + " ";
            }
            //res += x.search(word) + " ";
            //res += spt.get(word) + " ";
        }
                
        System.out.println("----------------------------------");
        System.out.println("Traduccion del documento.");
        System.out.println(res);
        System.out.println("----------------------------------");    
            
            } catch (IOException ex) {
                System.out.println("ERROR al leer archivo.");
            }   
        
        } else{ 
            System.out.println("Operacion cancelada.");
            System.exit(0);
        }
    }
    
    
}

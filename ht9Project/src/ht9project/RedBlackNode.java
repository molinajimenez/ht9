/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht9project;

    public class RedBlackNode<E>{
        private Association<String, String> asoc;
        public RedBlackNode<E> parent, left, right;  // links to left and right subtrees
        private boolean color;     // color of parent link
        private int size;          // subtree count
        
        public RedBlackNode(String key, String value, boolean color, int size) {
            asoc = new Association<>(key, value);
            this.color = color;
            this.size = size;
            parent = null; 
            left = right = null;
        }
        public boolean getColor(){
            return this.color;
        }
        public int getSize(){
            return this.size;
        }
        public String getValue(){
            return this.asoc.getValue();
        }
        public String getKey(){
            return this.asoc.getKey();
        }
        public void setColor(boolean x){
            this.color = x;
        }
        public void setValue(String x){
            this.asoc.theValue=x;
        }
        public void setKey(String x){
            this.asoc.theKey=x;
        }
        public void setSize(int x){
            this.size=x;
        }
         /**
     * Obtiene izq
     * @return Node hijo izquierdo del nodo
     */
    public RedBlackNode<E> getLeft()
    {
        return left;
    }
    /**
     * Obtiene derecha
     * @return Node hijo derecho del nodo
     */
    public RedBlackNode<E> getRight()
    {
        return right;
    }
    /**
     * Obtiene referencia a padre
     * @return Node padre del nodo
     */
    public RedBlackNode<E> getParent()
    {
        return parent;
    }
    /**
     * set al izq
     * @param left: nueva referencia a hijo derecho del nodo
     */
    public void setLeft(RedBlackNode left)
    {
        this.left = left;
    }
    /**
     * set al derecho
     * @param right: nueva referencia a hijo izquierdo del nodo
     */
    public void setRight(RedBlackNode right)
    {
        this.right = right;
    }
    /**
     * set al padre
     * @param newParent: nuevo padre del nodo
     */
    public void setParent(RedBlackNode newParent)
    // post: re-parents this node to parent reference, or null
    {
            parent = newParent;
    }
    
     public String search(String value){
        if (value.equals(this.asoc.getKey())){
            
            return this.asoc.theValue;
        }else if (value.compareTo(this.asoc.getKey()) < 0) {
            
            if (left == null){
                return "*" + value + "*";
            }else{
                return left.search(value);
            }
        }else if(value.compareTo(this.asoc.getKey()) > 0) {
            
            if (right == null){
                return "*" + value + "*";
            }else{
                return right.search(value);

            }
        }
        
        return "*" + value + "*";
    }
        
    }
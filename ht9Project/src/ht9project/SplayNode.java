/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht9project;

/**
 *
 * @author USER
 */
public class SplayNode<E> {
    SplayNode<E> left;
    SplayNode<E> right;
    SplayNode<E> parent;
    Association<String, String> elemento;
    
    public SplayNode(Association<String, String> palabra, SplayNode left, SplayNode right, SplayNode parent){ 
        this.left = left;
        this.right = right;
        this.parent = parent;
        this.elemento = palabra;
    }
    
    public SplayNode(Association<String, String> palabra){ 
        this(palabra, null, null, null);
    }
    
    public SplayNode(){ 
        this(null, null, null, null);
    }
    
    public String getKey(){ 
        return elemento.theKey;
    }
    
    public String getValue(){ 
        return elemento.theValue;
    }
}
